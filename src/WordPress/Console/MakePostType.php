<?php

namespace TenDegrees\WordPress\Console;

use Illuminate\Support\Str;
use TenDegrees\Console\GeneratorCommand;

class MakePostType extends GeneratorCommand
{
    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'PostType';

    /**
     * The command signature.
     *
     * @var string
     */
    protected $signature = 'make:post-type {name : The post type name}
                                           {--force : Overwrite the post type if it exists}';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'Make a post type';

    /**
     * Replace the class name for the given stub.
     *
     * @param string $stub The stub contents
     * @param string $name The classname to replace
     *
     * @return string
     */
    protected function replaceClass(string $stub, string $name): string
    {
        $stub = parent::replaceClass($stub, $name);

        $postType = $this->argument('name');

        return str_replace(
            ['{{ postType }}', '{{ plural }}', '{{ singular }}'],
            [Str::lower($postType), Str::plural($postType), $postType],
            $stub
        );
    }

    /**
     * Get the stub path.
     *
     * @return string
     */
    protected function getStub(): string
    {
        return __DIR__ . '/stubs/post-type.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param string $rootNamespace The root namespace
     *
     * @return string
     */
    protected function getDefaultNamespace(string $rootNamespace): string
    {
        return $rootNamespace . '\\WordPress\\PostTypes';
    }
}
