<?php

namespace TenDegrees\WordPress\Console;

use Illuminate\Support\Str;
use TenDegrees\Console\GeneratorCommand;

class MakeAdminPage extends GeneratorCommand
{
    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'AdminPage';

    /**
     * The command signature.
     *
     * @var string
     */
    protected $signature = 'make:admin-page {name : The admin page name}
                                            {--force : Overwrite the admin page if it exists}';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'Make an admin page';

    /**
     * Replace the class name for the given stub.
     *
     * @param string $stub The stub contents
     * @param string $name The classname to replace
     *
     * @return string
     */
    protected function replaceClass(string $stub, string $name): string
    {
        $stub = parent::replaceClass($stub, $name);

        $title = Str::of($this->argument('name'))->snake->title->replace('_', ' ');

        return str_replace('{{ title }}', $title, $stub);
    }

    /**
     * Get the stub path.
     *
     * @return string
     */
    protected function getStub(): string
    {
        return __DIR__ . '/stubs/admin-page.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param string $rootNamespace The root namespace
     *
     * @return string
     */
    protected function getDefaultNamespace(string $rootNamespace): string
    {
        return $rootNamespace . '\\WordPress\\AdminPages';
    }
}
