<?php

namespace TenDegrees\WordPress;

use TenDegrees\Support\Facades\View;
use Illuminate\Support\Str;

class AdminPage
{
    /**
     * The parent page slug
     *
     * @var string|null
     */
    protected static $parentPage = null;

    /**
     * The menu page capability
     *
     * @var string
     */
    protected static $capability = 'manage_options';

    /**
     * The menu position
     *
     * @var int|null
     */
    protected static $position = null;

    /**
     * Return the page title
     *
     * @return string
     */
    public function pageTitle()
    {
        return __('Page Title', '@textdomain');
    }

    /**
     * Return the menu title
     *
     * @return string
     */
    public function menuTitle()
    {
        return $this->pageTitle();
    }

    /**
     * Return the page slug
     *
     * @return string
     */
    public function slug()
    {
        return Str::slug($this->pageTitle());
    }

    /**
     * Return the page icon
     *
     * @return string
     */
    public function icon()
    {
        return 'none';
    }

    /**
     * Create the admin page instance
     */
    public function __construct()
    {
        add_action('admin_menu', [$this, 'register']);
    }

    /**
     * Registers a new settings page.
     *
     * @return void
     */
    public function register()
    {
        if (static::$parentPage) {
            $this->addSubMenuPage();
        } else {
            $this->addMenuPage();
        }
    }

    /**
     * Add the menu page
     *
     * @return void
     */
    public function addMenuPage()
    {
        add_menu_page(
            $this->pageTitle(),
            $this->menuTitle(),
            static::$capability,
            $this->slug(),
            [$this, 'renderWrapper'],
            $this->icon(),
            static::$position
        );
    }

    /**
     * Add the sub menu page
     *
     * @return void
     */
    public function addSubMenuPage()
    {
        add_submenu_page(
            static::$parentPage,
            $this->pageTitle(),
            $this->menuTitle(),
            static::$capability,
            $this->slug(),
            [$this, 'renderWrapper'],
            static::$position
        );
    }

    /**
     * Render the page with a wrapper
     *
     * @return void
     */
    public function renderWrapper()
    {
        echo "<div class=\"wrap\"><h1>{$this->pageTitle()}</h1>{$this->render($this)}</div>";
    }

    /**
     * Render the page
     *
     * @return string
     */
    public function render()
    {
        return '';
    }

    /**
     * Return a view
     *
     * @param string $path
     * @param array $args
     * @return string
     */
    public function view(string $path, array $args = [])
    {
        return View::make($path, $args)->render();
    }
}
