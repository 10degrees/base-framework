<?php

namespace TenDegrees\WordPress;

use TenDegrees\Foundation\Application;

abstract class Shortcode
{
    /**
     * The application instance
     *
     * @var \TenDegrees\Foundation\Application
     */
    protected $app;

    /**
     * The shortcode name
     *
     * @var string
     */
    protected $shortcode = '';

    /**
     * The default attributes
     *
     * @var array
     */
    protected $defaultAttributes = [];

    /**
     * The shortcode attributes
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * The shortcode content
     *
     * @var string
     */
    protected $content = '';

    /**
     * Add the shortcode
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Register the shortcode
     *
     * @return void
     */
    public function register()
    {
        add_shortcode($this->shortcode, [$this, 'setVariables']);
    }

    /**
     * Set shortcode atts
     *
     * @param array|string|null $attrs   The shortcode attributes
     * @param string|null       $content The shortcode content
     *
     * @return string
     */
    public function setVariables($attrs = [], ?string $content = null): string
    {
        if ($this->defaultAttributes) {
            $this->attributes = shortcode_atts($this->defaultAttributes, $attrs);
        }

        $this->content = $content ?? '';

        return $this->app->call([$this, 'handle']);
    }

    /**
     * Dynamically get the shortcode attributes
     *
     * @param string $key The attribute name
     *
     * @return mixed
     */
    public function __get(string $key)
    {
        return $this->attributes[$key] ?? null;
    }
}
