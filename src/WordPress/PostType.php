<?php

namespace TenDegrees\WordPress;

abstract class PostType
{
    /**
     * Constructor
     */
    public function __construct()
    {
        add_action('init', [$this, 'register']);
    }

    /**
     * Register the post type and taxonomies
     *
     * @return void
     */
    public function register()
    {
        $this->registerPostType();

        if (method_exists($this, 'registerTaxonomies')) {
            call_user_func([$this, 'registerTaxonomies']);
        }
    }

    /**
     * Register the post type
     *
     * @return void
     */
    abstract public function registerPostType();
}
