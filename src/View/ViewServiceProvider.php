<?php

namespace TenDegrees\View;

use TenDegrees\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register the services
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('view', function ($app) {
            return new View($app->basePath('resources/views'));
        });
    }
}
