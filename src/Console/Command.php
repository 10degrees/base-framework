<?php

namespace TenDegrees\Console;

use WP_CLI;
use function WP_CLI\Utils\format_items as wpcli_format_items;

abstract class Command
{
    /**
     * The command signature.
     *
     * @var string
     */
    protected $signature;

    /**
     * The command name.
     *
     * @var string
     */
    protected $name;

    /**
     * The command description.
     *
     * @var string
     */
    protected $description;

    /**
     * The command synopsis.
     *
     * @var array
     */
    protected $synopsis = [];

    /**
     * The allowed arguments.
     *
     * @var array
     */
    protected $allowedArguments = [];

    /**
     * The console arguments.
     *
     * @var array
     */
    protected $arguments = [];

    /**
     * The console options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * Create the command instance
     */
    public function __construct()
    {
        $this->parseSignature();
    }

    /**
     * Register the WP_CLI command.
     *
     * @return void
     */
    public function register()
    {
        WP_CLI::add_command('td ' . $this->name, $this, $this->synopsis);
    }

    /**
     * Parse the signature into the command name arguments, options and synopsis.
     *
     * @return void
     */
    protected function parseSignature()
    {
        [$name, $arguments, $options] = Parser::parse($this->signature);

        $this->name = $name;

        $this->allowedArguments = $arguments;

        $this->synopsis = [
            'shortdesc' => $this->description,
            'synopsis'  => array_merge($arguments, $options),
        ];
    }

    /**
     * Set up the command arguments and options.
     *
     * @param array $args
     * @param array $options
     * @return void
     */
    public function __invoke(array $args, array $options)
    {
        $this->arguments = $this->parseArguments($args);

        $this->options = $options;

        $this->handle();
    }

    /**
     * Handle the command call.
     *
     * @return void
     */
    abstract protected function handle();

    /**
     * Parse the arguments into a keyed array.
     *
     * @param array $args
     * @return array
     */
    protected function parseArguments(array $args): array
    {
        $arguments = [];

        foreach ($this->allowedArguments as $index => $argument) {
            $arguments[$argument['name']] = $args[$index];
        }

        return $arguments;
    }

    /**
     * Get all the arguments.
     *
     * @return array
     */
    protected function arguments(): array
    {
        return $this->arguments;
    }

    /**
     * Get a named argument.
     *
     * @param string $key
     * @return mixed
     */
    protected function argument(string $key)
    {
        return $this->arguments[$key] ?? null;
    }

    /**
     * Get all the options.
     *
     * @return array
     */
    protected function options(): array
    {
        return $this->options;
    }

    /**
     * Get an option.
     *
     * @param string $key
     * @return mixed
     */
    protected function option(string $key)
    {
        return $this->options[$key] ?? null;
    }

    /**
     * Send an success to the console.
     *
     * @param string $message
     * @return self
     */
    protected function success(string $message)
    {
        WP_CLI::success($message);

        return $this;
    }

    /**
     * Send a warning to the console.
     *
     * @param string $message
     * @return self
     */
    protected function warning(string $message)
    {
        WP_CLI::warning($message);

        return $this;
    }

    /**
     * Send an error to the console.
     *
     * @param string $message
     * @return void
     */
    protected function error(string $message)
    {
        WP_CLI::error($message);
    }

    /**
     * Send an message to the console.
     *
     * @param string $message
     * @return self
     */
    protected function line(string $message)
    {
        WP_CLI::log($message);

        return $this;
    }

    /**
     * Output new lines
     *
     * @param int $lines
     * @return self
     */
    protected function newLine(int $lines = 1)
    {
        $this->line(str_repeat(PHP_EOL, $lines));

        return $this;
    }

    /**
     * Output a table
     *
     * @param array $headers
     * @param array $data
     * @return self
     */
    protected function table(array $headers, array $data)
    {
        wpcli_format_items('table', $data, $headers);

        return $this;
    }

    /**
     * Create a new progress bar
     *
     * @param integer $count
     * @return \TenDegrees\Console\ProgressBar
     */
    public function createProgressBar(int $count)
    {
        return new ProgressBar($count);
    }

    /**
     * Ask the user to confirm an action.
     *
     * @param string $question
     * @param bool   $skip
     * @return bool
     */
    protected function confirm(string $question, bool $skip = false): bool
    {
        if (!$skip) {
            $answer = $this->ask($question . ' [y/n]');

            return in_array(strtolower($answer), ['y', 'yes']);
        }
        return true;
    }

    /**
     * Prompt the user with a question and return their answer.
     *
     * @param string $question
     * @return string
     */
    protected function ask(string $question)
    {
        fwrite(STDOUT, $question . ' ');

        return trim(fgets(STDIN));
    }

    /**
     * Terminate the console with an optional line output.
     *
     * @param string|null $message
     * @return void
     */
    protected function terminate(?string $message = null)
    {
        if ($message) {
            $this->line($message);
        }
        die;
    }

    /**
     * Call a command
     *
     * @param string $command
     * @param array $args
     * @return void
     */
    protected function call(string $command, array $args = [])
    {
        $args = join(' ', array_map(function ($key, $value) {
            if (substr($key, 0, 2) === "--") {
                if ($value === true) {
                    return $key;
                } elseif ($value === false) {
                    return '';
                } else {
                    return $key . '=' . $value;
                }
            } else {
                return $value;
            }
        }, array_keys($args), $args));

        WP_CLI::runcommand($command . ' ' . $args);
    }
}
