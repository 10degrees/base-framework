<?php

namespace TenDegrees\Routing;

use ArrayObject;
use Closure;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Http\JsonResponse;
use JsonSerializable;
use Symfony\Component\HttpFoundation\Response;
use TenDegrees\Support\Pipeline;

class Router
{
    /**
     * The application instance
     *
     * @var \TenDegrees\Foundation\Application
     */
    protected $app;

    /**
     * Create the router isntance
     *
     * @param \TenDegrees\Foundation\Application $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * Call the controller action
     *
     * @param callable $action
     * @param array $parameters
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function callAction($action, array $parameters = [])
    {
        if ($action instanceof Closure) {
            return $this->prepareResponse(
                $this->app->call($action, $parameters)
            );
        }

        [$controller, $method] = $this->resolveControllerClass($action);

        $response = (new Pipeline())
            ->send($this->app['request'])
            ->through($controller->getMiddleware())
            ->then(function ($request) use ($parameters, $controller, $method) {
                return $this->prepareResponse(
                    $this->app->call([$controller, $method], $parameters)
                );
            });

        return $this->prepareResponse($response);
    }

    /**
     * Create a response instance from the given value.
     *
     * @param  mixed  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function prepareResponse($response)
    {
        if ($response instanceof Response) {
            return $response;
        }

        if (
            $response instanceof Arrayable ||
            $response instanceof Jsonable ||
            $response instanceof ArrayObject ||
            $response instanceof JsonSerializable ||
            is_array($response)
        ) {
            return new JsonResponse($response);
        }

        return new Response($response, 200, ['Content-Type' => 'text/html']);
    }

    /**
     * Resolve the controller class and return the controller and the method.
     *
     * @param string|array $action
     * @return array
     */
    protected function resolveControllerClass($action)
    {
        if (is_array($action)) {
            return [$this->app->make($action[0]), $action[1]];
        }

        return [$this->app->make($action), '__invoke'];
    }

    /**
     * Dynamically call the ajax or rest router
     *
     * @param string $method
     * @param array $parameters
     * @return mixed
     */
    public function __call(string $method, array $parameters = [])
    {
        if ($method === 'ajax') {
            return $this->app->make('router.ajax')->$method(...$parameters);
        }

        return $this->app->make('router.rest')->$method(...$parameters);
    }
}
