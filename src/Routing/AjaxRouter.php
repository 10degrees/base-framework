<?php

namespace TenDegrees\Routing;

class AjaxRouter
{
    /**
     * The router instance
     *
     * @var \TenDegrees\Routing\Router
     */
    protected $router;

    /**
     * Create the ajax router instance
     *
     * @param \TenDegrees\Routing\Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * Run the ajax method
     *
     * @param string $uri
     * @param callable $action
     * @return void
     */
    public function ajax(string $uri, $action)
    {
        add_action("wp_ajax_{$uri}", $this->resolveAction($action));
        add_action("wp_ajax_nopriv_{$uri}", $this->resolveAction($action));
    }

    /**
     * Resolve the controller action
     *
     * @param callable $action
     * @return \Closure
     */
    protected function resolveAction($action)
    {
        return function () use ($action) {
            $this->router->callAction($action)->send();
            die;
        };
    }
}
