<?php

namespace TenDegrees\Routing;

use TenDegrees\Support\ServiceProvider;

class RoutingServiceProvider extends ServiceProvider
{
    /**
     * Register the services
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('router', function ($app) {
            return new Router($app);
        });
        $this->app->singleton('router.ajax', function ($app) {
            return new AjaxRouter($app['router']);
        });
        $this->app->singleton('router.rest', function ($app) {
            return new RestRouter($app['router'], $app['request']);
        });
        $this->app->singleton('url', function ($app) {
            return new UrlGenerator($app['request']);
        });
    }
}
