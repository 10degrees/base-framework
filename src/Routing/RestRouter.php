<?php

namespace TenDegrees\Routing;

use TenDegrees\Http\Request;
use WP_REST_Request;

class RestRouter
{
    /**
     * The router instance
     *
     * @var \TenDegrees\Routing\Router
     */
    protected $router;

    /**
     * The request instance
     *
     * @var \TenDegrees\Http\Request
     */
    protected $request;

    /**
     * The REST route namespace
     *
     * @var string
     */
    protected $namespace = 'api';

    /**
     * Create the rest router instance
     *
     * @param \TenDegrees\Routing\Router $router
     * @param \TenDegrees\Http\Request $request
     */
    public function __construct(Router $router, Request $request)
    {
        $this->router = $router;
        $this->request = $request;
    }

    /**
     * Run the get method
     *
     * @param string $uri
     * @param callable $action
     * @return void
     */
    public function get(string $uri, $action)
    {
        return $this->call(['GET', 'HEAD'], $uri, $action);
    }

    /**
     * Run the post method
     *
     * @param string $uri
     * @param callable $action
     * @return void
     */
    public function post(string $uri, $action)
    {
        return $this->call(['POST'], $uri, $action);
    }

    /**
     * Run the put method
     *
     * @param string $uri
     * @param callable $action
     * @return void
     */
    public function put(string $uri, $action)
    {
        return $this->call(['PUT'], $uri, $action);
    }

    /**
     * Run the patch method
     *
     * @param string $uri
     * @param callable $action
     * @return void
     */
    public function patch(string $uri, $action)
    {
        return $this->call(['PATCH'], $uri, $action);
    }

    /**
     * Run the delete method
     *
     * @param string $uri
     * @param callable $action
     * @return void
     */
    public function delete(string $uri, $action)
    {
        return $this->call(['DELETE'], $uri, $action);
    }

    /**
     * Run the options method
     *
     * @param string $uri
     * @param callable $action
     * @return void
     */
    public function options(string $uri, $action)
    {
        return $this->call(['OPTIONS'], $uri, $action);
    }

    /**
     * Matches the given methods
     *
     * @param array $methods
     * @param string $uri
     * @param callable $action
     * @return void
     */
    public function matches(array $methods, string $uri, $action)
    {
        if (in_array('GET', $methods)) {
            $methods[] = 'HEAD';
        }

        return $this->call(array_unique($methods), $uri, $action);
    }

    /**
     * Run any method
     *
     * @param string $uri
     * @param callable $action
     * @return void
     */
    public function any(string $uri, $action)
    {
        return $this->call(['GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE', 'OPTIONS'], $uri, $action);
    }

    /**
     * Register the rest route
     *
     * @param array $methods
     * @param string $uri
     * @param callable $action
     * @return void
     */
    protected function call(array $methods, string $uri, $action)
    {
        add_action('rest_api_init', function () use ($methods, $uri, $action) {
            register_rest_route($this->namespace, $this->parseUri($uri), [
                'methods'             => $methods,
                'callback'            => $this->resolveAction($action),
                'permission_callback' => '__return_true',
            ]);
        });
    }

    /**
     * Parse the URI
     *
     * @param string $uri
     * @return string
     */
    protected function parseUri(string $uri)
    {
        return preg_replace('@\/\{([\w]+?)(\?)?\}@', '\/?(?P<$1>[\w-]+)$2', $uri);
    }

    /**
     * Resolve the controller action
     *
     * @param callable $action
     * @return \Closure
     */
    protected function resolveAction($action)
    {
        return function (WP_REST_Request $request) use ($action) {
            $this->request->merge($parameters = $request->get_url_params());

            $this->router->callAction($action, $parameters)->send();
            die;
        };
    }

    /**
     * Set the REST route namespace
     *
     * @param string $namespace
     * @return void
     */
    public function setNamespace(string $namespace)
    {
        $this->namespace = $namespace;
    }
}
