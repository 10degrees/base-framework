<?php

namespace TenDegrees\Routing;

use TenDegrees\Support\Facades\View;

abstract class Controller
{
    /**
     * The middleware registered on the controller.
     *
     * @var array
     */
    protected $middleware = [];

    /**
     * Register middleware on the controller.
     *
     * @param \Closure|array|string $middleware
     */
    public function middleware($middleware)
    {
        foreach ((array) $middleware as $m) {
            $this->middleware[] = $m;
        }
    }

    /**
     * Get the middleware assigned to the controller.
     *
     * @return array
     */
    public function getMiddleware()
    {
        return $this->middleware;
    }

    /**
     * Make a view.
     *
     * @param string $path
     * @param array $args
     * @return string
     */
    public function view(string $path, array $args = [])
    {
        return View::make($path, $args)->render();
    }
}
