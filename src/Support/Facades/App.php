<?php

namespace TenDegrees\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static void register(\TenDegrees\Support\ServiceProvider|string $provider) Register a service provider
 * @method static void boot() Boot the application
 * @method static string basePath(string $path = '') Get the base path for the app.
 * @method static string|bool environment(string ...$args) Get or check the current environment.
 * @method static bool isLocal() Determine if application is in local environment.
 * @method static bool isDevelopment() Determine if application is in development environment.
 * @method static bool isStaging() Determine if application is in staging environment.
 * @method static bool isProduction() Determine if application is in production environment.
 * @method static bool runningInConsole() Determine if the app is running in the console.
 * @method static string getNamespace() Get the application namespace
 *
 * @see \TenDegrees\Foundation\Application
 */
class App extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'app';
    }
}
