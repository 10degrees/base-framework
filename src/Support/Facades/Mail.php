<?php

namespace TenDegrees\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static string to(mixed $users) Set the to addresses
 * @method static string cc(mixed $users) Set the cc addresses
 * @method static string bcc(mixed $users) Set the bcc addresses
 * @method static bool send(\TenDegrees\Mail\Mailer $mailer) Send the mail
 * @method static string render(\TenDegrees\Mail\Mailer $mailer) Render the email
 *
 * @see \TenDegrees\Mail\MailFactory
 */
class Mail extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'mail';
    }
}
