<?php

namespace TenDegrees\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static void ajax(string $uri, callable $action) Run the ajax method
 * @method static void get(string $uri, callable $action) Run the get method
 * @method static void post(string $uri, callable $action) Run the post method
 * @method static void put(string $uri, callable $action) Run the put method
 * @method static void patch(string $uri, callable $action) Run the patch method
 * @method static void delete(string $uri, callable $action) Run the delete method
 * @method static void options(string $uri, callable $action) Run the options method
 * @method static void matches(array $methods, string $uri, callable $action) Matches the given methods
 * @method static void any(string $uri, callable $action) Run any method
 * @method static void setNamespace(string $namespace) Set the REST route namespace
 *
 * @see \TenDegrees\Routing\Router, \TenDegrees\Routing\AjaxRouter, \TenDegrees\Routing\RestRouter
 */
class Route extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'router';
    }
}
