<?php

namespace TenDegrees\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static \TenDegrees\View\View make(string $view, array $args = []) Make a view.
 * @method static string get(string $view, array $args = []) Get the view.
 *
 * @see \TenDegrees\View\View
 */
class View extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'view';
    }
}
