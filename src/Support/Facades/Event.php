<?php

namespace TenDegrees\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static void listen(string|array $events, \Closure|string|null $listener = null) Register an event listener with the dispatcher.
 * @method static bool hasListeners(string $eventName) Determine if a given event has listeners.
 * @method static array|null dispatch(string|object $event, mixed $payload = []) Fire an event and call the listeners.
 * @method static void subscribe(object|string $subscriber) Register an event subscriber with the dispatcher.
 *
 * @see \TenDegrees\Events\Dispatcher
 */
class Event extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'events';
    }
}
