<?php

namespace TenDegrees\Support;

use TenDegrees\Foundation\Application;

abstract class ServiceProvider
{
    /**
     * The app instance
     *
     * @var \TenDegrees\Foundation\Application
     */
    protected $app;

    /**
     * Create the provider instance
     *
     * @param \TenDegrees\Foundation\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Register the services
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
