<?php

namespace TenDegrees\Foundation;

use Exception;

class Mix
{
    /**
     * The app instance
     *
     * @var \TenDegrees\Foundation\Application
     */
    protected $app;

    /**
     * The stored manifests
     *
     * @var array
     */
    protected static $manifests = [];

    /**
     * Create the mix class
     *
     * @param \TenDegrees\Foundation\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Get the path to a versioned Mix file.
     *
     * @param  string  $path
     * @param  string  $assetUrl
     * @return string
     *
     * @throws \Exception
     */
    public function __invoke(string $path, string $assetUrl)
    {
        $manifestPath = $this->app->basePath('dist/mix-manifest.json');

        if (!isset(static::$manifests[$manifestPath])) {
            if (!is_file($manifestPath)) {
                throw new Exception('The Mix manifest does not exist.');
            }

            static::$manifests[$manifestPath] = json_decode(file_get_contents($manifestPath), true);
        }

        $manifest = static::$manifests[$manifestPath];

        $path = '/' . ltrim(ltrim($path, '/'), 'dist/');

        if (!isset($manifest[$path])) {
            throw new Exception("Unable to locate Mix file: {$path}.");
        }

        return trim($assetUrl, '/') . '/dist' . $manifest[$path];
    }
}
