<?php

namespace TenDegrees\Foundation\Providers;

use TenDegrees\Support\ServiceProvider;

class WordPressServiceProvider extends ServiceProvider
{
    /**
     * The post types to register
     *
     * @var array
     */
    protected $postTypes = [];

    /**
     * The shortcodes to register
     *
     * @var array
     */
    protected $shortcodes = [];

    /**
     * The admin pages to register
     *
     * @var array
     */
    protected $adminPages = [];

    /**
     * Boot the services
     *
     * @return void
     */
    public function boot()
    {
        foreach ($this->postTypes as $postType) {
            $this->app->make($postType);
        }

        foreach ($this->shortcodes as $shortcode) {
            $this->app->make($shortcode)->register();
        }

        foreach ($this->adminPages as $page) {
            $this->app->make($page);
        }
    }
}
