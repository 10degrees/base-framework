<?php

namespace TenDegrees\Foundation\Providers;

use TenDegrees\Support\ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The hooks (actions/filters) to "listen" to.
     *
     * @var array
     */
    protected $hooks = [];

    /**
     * The subscribers. These are passed the event dispatcher instance.
     *
     * @var array
     */
    protected $subscribers = [];

    /**
     * Boot the services
     *
     * @return void
     */
    public function boot()
    {
        $dispatcher = $this->app->make('events');

        foreach ($this->hooks as $event => $listeners) {
            foreach (array_unique($listeners) as $listener) {
                $dispatcher->listen($event, $listener);
            }
        }

        foreach ($this->subscribers as $subscriber) {
            $dispatcher->subscribe($subscriber);
        }
    }
}
