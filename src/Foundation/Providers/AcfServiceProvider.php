<?php

namespace TenDegrees\Foundation\Providers;

use TenDegrees\Support\ServiceProvider;

class AcfServiceProvider extends ServiceProvider
{
    /**
     * The blocks to register
     *
     * @var array
     */
    protected $blocks = [];

    /**
     * The field groups to register
     *
     * @var array
     */
    protected $fieldGroups = [];

    /**
     * Boot the services
     *
     * @return void
     */
    public function boot()
    {
        foreach ($this->blocks as $block) {
            $this->app->make($block)->register();
        }

        foreach ($this->fieldGroups as $fieldGroup) {
            $this->app->make($fieldGroup)->register();
        }
    }
}
