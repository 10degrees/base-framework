<?php

namespace TenDegrees\Foundation\Providers;

use ReflectionClass;
use Symfony\Component\Finder\Finder;
use TenDegrees\Console\Command;
use TenDegrees\Support\ServiceProvider;

class ConsoleServiceProvider extends ServiceProvider
{
    /**
     * The console commands to register
     *
     * @var array
     */
    protected $commands = [
        \TenDegrees\Acf\Console\MakeBlock::class,
        \TenDegrees\Acf\Console\MakeFieldGroup::class,
        \TenDegrees\Console\Console\MakeCommand::class,
        \TenDegrees\Database\Console\MakeModel::class,
        \TenDegrees\Events\Console\MakeListener::class,
        \TenDegrees\Events\Console\MakeSubscriber::class,
        \TenDegrees\Mail\Console\MakeMail::class,
        \TenDegrees\Routing\Console\MakeController::class,
        \TenDegrees\Routing\Console\MakeMiddleware::class,
        \TenDegrees\Support\Console\MakeProvider::class,
        \TenDegrees\WordPress\Console\MakeAdminPage::class,
        \TenDegrees\WordPress\Console\MakePostType::class,
        \TenDegrees\WordPress\Console\MakeShortcode::class,
    ];

    /**
     * Register the services
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->commands as $command) {
            $this->app->singleton($command);
        }
    }

    /**
     * Boot the services
     *
     * @return void
     */
    public function boot()
    {
        foreach ($this->commands as $command) {
            $this->app->make($command)->register();
        }

        $this->load($this->app->basePath('app/Console'));
    }

    /**
     * Load commands from the console directory
     *
     * @param string $path The directory paths to load
     *
     * @return void
     */
    protected function load(string $path): void
    {
        if (!is_dir($path)) {
            return;
        }

        $namespace = $this->app->getNamespace() . 'Console\\';

        foreach ((new Finder)->in($path)->files() as $command) {
            $command = $namespace . str_replace(
                ['/', '.php'],
                ['\\', ''],
                $command->getRelativePathname()
            );

            if (is_subclass_of($command, Command::class) && !(new ReflectionClass($command))->isAbstract()) {
                $this->app->make($command)->register();
            }
        }
    }
}
