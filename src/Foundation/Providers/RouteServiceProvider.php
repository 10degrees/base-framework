<?php

namespace TenDegrees\Foundation\Providers;

use TenDegrees\Support\Facades\Route;
use TenDegrees\Support\ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The REST namespace
     *
     * @var array
     */
    protected static $restNamespace = 'api';

    /**
     * Boot the services
     *
     * @return void
     */
    public function boot()
    {
        Route::setNamespace(static::$restNamespace);

        if (method_exists($this, 'map')) {
            $this->app->call([$this, 'map']);
        }
    }
}
