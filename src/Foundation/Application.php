<?php

namespace TenDegrees\Foundation;

use Illuminate\Container\Container;
use Illuminate\Support\Facades\Facade;
use RuntimeException;
use TenDegrees\Events\EventServiceProvider;
use TenDegrees\Filesystem\Filesystem;
use TenDegrees\Foundation\Providers\ConsoleServiceProvider;
use TenDegrees\Http\Request;
use TenDegrees\Support\ServiceProvider;

class Application extends Container
{
    /**
     * The application base path
     *
     * @var string|null
     */
    protected $basePath;

    /**
     * The loaded providers
     *
     * @var \TenDegrees\Support\ServiceProvider[]
     */
    protected $loadedProviders = [];

    /**
     * Indicates if the app has booted
     *
     * @var bool
     */
    protected $booted = false;

    /**
     * The app namespace
     *
     * @var string
     */
    protected static $namespace;

    /**
     * Create the application instance
     *
     * @param string|null $basePath
     */
    public function __construct(?string $basePath = null)
    {
        $this->basePath = $basePath;

        $this->registerCoreBindings();
        $this->registerCoreProviders();
        $this->registerContainerAliases();

        Facade::setFacadeApplication($this);
    }

    /**
     * Register the core bindings in the container
     *
     * @return void
     */
    protected function registerCoreBindings()
    {
        static::setInstance($this);

        $this->instance('app', $this);
        $this->instance(self::class, $this);

        $this->instance('env', $this->environment());

        $this->instance('request', Request::capture());

        $this->singleton(Mix::class);

        $this->app->singleton('files', function () {
            return new Filesystem();
        });
    }

    /**
     * Register the core service providers
     *
     * @return void
     */
    protected function registerCoreProviders()
    {
        $this->register(EventServiceProvider::class);

        if ($this->runningInConsole()) {
            $this->register(ConsoleServiceProvider::class);
        }
    }

    /**
     * Register the container aliases
     *
     * @return void
     */
    protected function registerContainerAliases()
    {
        foreach ([
            'app' => [
                self::class,
                \Illuminate\Container\Container::class,
                \Illuminate\Contracts\Container\Container::class,
                \Psr\Container\ContainerInterface::class,
            ],
            'files' => [
                \TenDegrees\Filesystem\Filesystem::class,
                \Illuminate\Filesystem\Filesystem::class,
            ],
            'request' => [
                \TenDegrees\Http\Request::class,
                \Illuminate\Http\Request::class,
            ],
            'router' => [
                \TenDegrees\Routing\Router::class,
            ],
            'router.ajax' => [
                \TenDegrees\Routing\AjaxRouter::class,
            ],
            'router.rest' => [
                \TenDegrees\Routing\RestRouter::class,
            ],
            'url' => [
                \TenDegrees\Routing\UrlGenerator::class,
            ],
            'view' => [
                \TenDegrees\View\View::class,
            ],
        ] as $alias => $abstracts) {
            foreach ($abstracts as $abstract) {
                $this->alias($alias, $abstract);
            }
        }
    }

    /**
     * Register a service provider
     *
     * @param \TenDegrees\Support\ServiceProvider|string $provider
     * @return void
     */
    public function register($provider)
    {
        if (!$provider instanceof ServiceProvider) {
            $provider = new $provider($this);
        }

        if (array_key_exists($name = get_class($provider), $this->loadedProviders)) {
            return;
        }

        $this->loadedProviders[$name] = $provider;

        $provider->register();

        if ($this->booted) {
            $this->bootProvider($provider);
        }
    }

    /**
     * Boot the given service provider.
     *
     * @param \TenDegrees\Support\ServiceProvider $provider
     * @return mixed
     */
    protected function bootProvider(ServiceProvider $provider)
    {
        if (method_exists($provider, 'boot')) {
            return $this->call([$provider, 'boot']);
        }
    }

    /**
     * Boot the application
     *
     * @return void
     */
    public function boot()
    {
        if ($this->booted) {
            return;
        }

        foreach ($this->loadedProviders as $provider) {
            $this->bootProvider($provider);
        }

        $this->booted = true;
    }

    /**
     * Get the base path for the app.
     *
     * @param string $path
     * @return string
     */
    public function basePath(string $path = ''): string
    {
        return $this->basePath . ($path ? DIRECTORY_SEPARATOR . $path : $path);
    }

    /**
     * Get or check the current environment.
     *
     * @param string ...$args
     * @return string|bool
     */
    public function environment(string ...$args)
    {
        if (function_exists('wp_get_environment_type')) {
            $env = call_user_func('wp_get_environment_type');
        } else {
            $env = 'production';
        }

        return $args ? in_array($env, $args) : $env;
    }

    /**
     * Determine if application is in local environment.
     *
     * @return bool
     */
    public function isLocal(): bool
    {
        return $this['env'] === 'local';
    }

    /**
     * Determine if application is in development environment.
     *
     * @return bool
     */
    public function isDevelopment(): bool
    {
        return $this['env'] === 'development';
    }

    /**
     * Determine if application is in staging environment.
     *
     * @return bool
     */
    public function isStaging(): bool
    {
        return $this['env'] === 'staging';
    }

    /**
     * Determine if application is in production environment.
     *
     * @return bool
     */
    public function isProduction(): bool
    {
        return $this['env'] === 'production';
    }

    /**
     * Determine if the app is running in the console.
     *
     * @return bool
     */
    public function runningInConsole(): bool
    {
        return class_exists('WP_CLI');
    }

    /**
     * Get the application namespace
     *
     * @return string
     *
     * @throws \RuntimeException
     */
    public function getNamespace(): string
    {
        if (static::$namespace) {
            return static::$namespace;
        }

        $composer = json_decode(file_get_contents($this->basePath('composer.json')), true);

        if ($composer['autoload'] && $loaders = $composer['autoload']['psr-4']) {
            foreach ($loaders as $loader => $path) {
                if (realpath($this->basePath('app')) === realpath($this->basePath($path))) {
                    return static::$namespace = $loader;
                }
            }
        }

        throw new RuntimeException('Unable to detect application namespace.');
    }
}
