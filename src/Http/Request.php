<?php

namespace TenDegrees\Http;

use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Http\Request as BaseRequest;
use JsonSerializable;

class Request extends BaseRequest implements Jsonable, JsonSerializable
{
    /**
     * Convert the object to its JSON representation.
     *
     * @param  int  $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->all();
    }
}
