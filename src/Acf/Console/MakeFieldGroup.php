<?php

namespace TenDegrees\Acf\Console;

use TenDegrees\Console\GeneratorCommand;

class MakeFieldGroup extends GeneratorCommand
{
    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'FieldGroup';

    /**
     * The command signature.
     *
     * @var string
     */
    protected $signature = 'make:field-group {name : The field group name}
                                             {--force : Overwrite the field group if it exists}';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'Make an ACF field group';

    /**
     * Get the stub path.
     *
     * @return string
     */
    protected function getStub(): string
    {
        return __DIR__ . '/stubs/field-group.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param string $rootNamespace The root namespace
     *
     * @return string
     */
    protected function getDefaultNamespace(string $rootNamespace): string
    {
        return $rootNamespace . '\\Acf\\FieldGroups';
    }
}
