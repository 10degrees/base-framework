<?php

namespace TenDegrees\Acf;

use TenDegrees\Foundation\Application;
use TenDegrees\View\View;

abstract class Block
{
    /**
     * A unique name that identifies the block (without namespace)
     *
     * @var string
     */
    protected $name = '';

    /**
     * The view instance
     *
     * @var \TenDegrees\View\View
     */
    protected $view;

    /**
     * The app instance
     *
     * @var \TenDegrees\Foundation\Application
     */
    protected $app;

    /**
     * Create the block instance
     *
     * @param \TenDegrees\View\View $view
     * @param \TenDegrees\Foundation\Application $app
     */
    public function __construct(View $view, Application $app)
    {
        $this->view = $view;
        $this->app = $app;
    }

    /**
     * Register the block
     *
     * @return void
     */
    public function register(): void
    {
        if (function_exists('acf_register_block')) {
            call_user_func('acf_register_block', $this->getBlockOptions());
        }
    }

    /**
     * Get the options for block registration
     *
     * @return array
     */
    protected function getBlockOptions(): array
    {
        return array_merge(
            $this->options(),
            [
                'name'            => $this->name,
                'render_callback' => $this->resolveHandler(),
            ]
        );
    }

    /**
     * Resolve the handle method out of the container
     *
     * @return \Closure
     */
    protected function resolveHandler()
    {
        return function (array $block) {
            echo $this->app->call([$this, 'handle'], ['block' => $block]);
        };
    }

    /**
     * The block options
     *
     * @return array
     */
    protected function options()
    {
        return [];
    }
}
