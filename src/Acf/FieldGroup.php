<?php

namespace TenDegrees\Acf;

use TenDegrees\Foundation\Application;

abstract class FieldGroup
{
    /**
     * The app instance
     *
     * @var \TenDegrees\Foundation\Application
     */
    protected $app;

    /**
     * Create the field group instance
     *
     * @param \TenDegrees\Foundation\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Constructor
     */
    public function register()
    {
        if (function_exists('acf_add_local_field_group')) {
            call_user_func('acf_add_local_field_group', $this->fields());
        }
    }

    /**
     * The field group exported from ACF
     *
     * @return array
     */
    public function fields()
    {
        return [];
    }
}
