<?php

namespace TenDegrees\Database;

use Corcel\Database;
use TenDegrees\Support\ServiceProvider;

class DatabaseServiceProvider extends ServiceProvider
{
    /**
     * Register the services
     *
     * @return void
     */
    public function register()
    {
        global $wpdb;

        $capsule = Database::connect(
            [
                'database'  => DB_NAME,
                'username'  => DB_USER,
                'password'  => DB_PASSWORD,
                'host'      => DB_HOST,
                'prefix'    => $wpdb->prefix,
                'charset'   => DB_CHARSET,
                'collation' => DB_COLLATE ?: 'utf8_unicode_ci',
            ]
        );

        $this->app->bind('db', function ($app) use ($capsule) {
            return $capsule->getDatabaseManager();
        });

        $this->app->bind('db.connection', function ($app) {
            return $app['db']->connection();
        });
    }
}
