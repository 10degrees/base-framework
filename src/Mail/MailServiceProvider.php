<?php

namespace TenDegrees\Mail;

use TenDegrees\Support\ServiceProvider;

class MailServiceProvider extends ServiceProvider
{
    /**
     * Register the services
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('mail', function ($app) {
            return new MailFactory($app);
        });
    }
}
