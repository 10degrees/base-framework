<?php

namespace TenDegrees\Mail;

class MailFactory
{
    /**
     * The to addresses
     *
     * @var array
     */
    protected $to = [];

    /**
     * The cc addresses
     *
     * @var array
     */
    protected $cc = [];

    /**
     * The bcc addresses
     *
     * @var array
     */
    protected $bcc = [];

    /**
     * Set the to addresses
     *
     * @param mixed $users
     * @return static
     */
    public function to($users)
    {
        $this->to = $users;

        return $this;
    }

    /**
     * Set the cc addresses
     *
     * @param mixed $users
     * @return static
     */
    public function cc($cc)
    {
        $this->cc = $cc;

        return $this;
    }

    /**
     * Set the bcc addresses
     *
     * @param mixed $users
     * @return static
     */
    public function bcc($bcc)
    {
        $this->bcc = $bcc;

        return $this;
    }

    /**
     * Send the mail
     *
     * @param \TenDegrees\Mail\Mailer $mailer
     * @return bool
     */
    public function send(Mailer $mailer)
    {
        return $mailer->to($this->to)
            ->cc($this->cc)
            ->bcc($this->bcc)
            ->send();
    }

    /**
     * Render the email
     *
     * @param \TenDegrees\Mail\Mailer $mailer
     * @return string
     */
    public function render(Mailer $mailer)
    {
        return $mailer->render();
    }
}
