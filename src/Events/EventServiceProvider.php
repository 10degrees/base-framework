<?php

namespace TenDegrees\Events;

use TenDegrees\Support\ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * Register the services
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('events', function ($app) {
            return new Dispatcher($app);
        });
    }
}
